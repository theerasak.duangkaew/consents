﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Consent.Web.Models;

namespace Consent.Web.Context
{
    public partial class ConcentDbContext : DbContext
    {
        public ConcentDbContext()
        {
        }

        public ConcentDbContext(DbContextOptions<ConcentDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Title> Titles { get; set; }
        public virtual DbSet<Amphurs> Amphurs { get; set; }
        public virtual DbSet<Customer> Customer { get; set; }
        public virtual DbSet<Geography> Geography { get; set; }
        public virtual DbSet<Provinces> Provinces { get; set; }
        public virtual DbSet<Questionnaire> Questionnaire { get; set; }
        public virtual DbSet<Staff> Staff { get; set; }
        public virtual DbSet<Tambons> Tambons { get; set; }
        public virtual DbSet<User> User { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=tcp:consentdb.database.windows.net,1433;Initial Catalog=consents;Persist Security Info=False;User ID=consent-user;Password=P@ssc0de123;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Amphurs>(entity =>
            {
                entity.HasKey(e => e.AmphurId)
                    .HasName("PK__amphurs__9B13B9854E5C1238");

                entity.Property(e => e.AmphurId).ValueGeneratedNever();
            });

            modelBuilder.Entity<Customer>(entity =>
            {
                entity.Property(e => e.CustomerId).IsUnicode(false);

                entity.Property(e => e.FirstName).IsUnicode(false);

                entity.Property(e => e.LastName).IsUnicode(false);
            });

            modelBuilder.Entity<Geography>(entity =>
            {
                entity.Property(e => e.GeographyId).ValueGeneratedNever();
            });

            modelBuilder.Entity<Provinces>(entity =>
            {
                entity.HasKey(e => e.ProvinceId)
                    .HasName("PK__province__20A1177F81155732");

                entity.Property(e => e.ProvinceId).ValueGeneratedNever();
            });

            modelBuilder.Entity<Tambons>(entity =>
            {
                entity.HasKey(e => e.TamId)
                    .HasName("PK__tambons__39E67F39C28E3D28");

                entity.Property(e => e.TamId).ValueGeneratedNever();
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
