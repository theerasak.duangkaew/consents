﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Consent.Web.Models
{
    public class Title
    {
        public int Id { get; set; }

        [StringLength(4)]
        public string TitleCode { get; set; }

        [StringLength(20)]
        public string TitleName { get; set; }

    }
}
