﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Consent.Web.Models
{
    public partial class User
    {
        [StringLength(50)]
        public string BusinessPartner { get; set; }
        [Column("KubotaID")]
        [StringLength(50)]
        public string KubotaId { get; set; }
        [StringLength(50)]
        public string Title { get; set; }
        [StringLength(50)]
        public string FirstName { get; set; }
        [StringLength(50)]
        public string MiddleName { get; set; }
        [StringLength(50)]
        public string LastName { get; set; }
        [Column(TypeName = "date")]
        public DateTime? BirthDate { get; set; }
        [StringLength(50)]
        public string SearchTerm { get; set; }
        [Key]
        [Column("IDCard")]
        [StringLength(13)]
        public string Idcard { get; set; }
        [StringLength(50)]
        public string BranchCode { get; set; }
        [StringLength(100)]
        public string Description { get; set; }
        [StringLength(50)]
        public string HouseNumber { get; set; }
        [StringLength(50)]
        public string RoomNumber { get; set; }
        [StringLength(50)]
        public string Floor { get; set; }
        [StringLength(50)]
        public string BuildingCode { get; set; }
        [StringLength(50)]
        public string Supplement { get; set; }
        [StringLength(50)]
        public string Street2 { get; set; }
        [StringLength(50)]
        public string Street4 { get; set; }
        [StringLength(50)]
        public string Street { get; set; }
        [StringLength(50)]
        public string Tambon { get; set; }
        [StringLength(50)]
        public string District { get; set; }
        [StringLength(50)]
        public string Province { get; set; }
        [StringLength(5)]
        public string ZipCode { get; set; }
        [StringLength(10)]
        public string TelephoneNo { get; set; }
        [StringLength(50)]
        public string Extension { get; set; }
        [StringLength(10)]
        public string MobilePhone1 { get; set; }
        [StringLength(10)]
        public string MobilePhone2 { get; set; }
        [StringLength(50)]
        public string Email { get; set; }
        [Column("FacebookID")]
        [StringLength(50)]
        public string FacebookId { get; set; }
        [Column("LineID")]
        [StringLength(50)]
        public string LineId { get; set; }
        [StringLength(50)]
        public string Gmail { get; set; }
        [StringLength(50)]
        public string Notes { get; set; }
        public bool? Consent { get; set; }
        [StringLength(50)]
        public string Source { get; set; }
        [Column(TypeName = "text")]
        public string SignatureImg { get; set; }
    }
}
