﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Consent.Web.Models
{
    [Table("customer")]
    public partial class Customer
    {
        [Key]
        [StringLength(50)]
        public string CustomerId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        [StringLength(50)]
        public string Tel { get; set; }
    }
}
