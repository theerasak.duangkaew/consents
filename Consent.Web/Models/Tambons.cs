﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Consent.Web.Models
{
    [Table("tambons")]
    public partial class Tambons
    {
        [Key]
        [Column("tam_id")]
        public int TamId { get; set; }
        [Column("tambonCode")]
        public int TambonCode { get; set; }
        [Required]
        [Column("tambonNameTH")]
        [StringLength(50)]
        public string TambonNameTh { get; set; }
        [Column("zipcode")]
        public int Zipcode { get; set; }
        [Column("amphurCode")]
        public int AmphurCode { get; set; }
    }
}
