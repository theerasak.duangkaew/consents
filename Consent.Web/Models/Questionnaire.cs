﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Consent.Web.Models
{
    public partial class Questionnaire
    {
        [Key]
        [Column("QuestionnaireID")]
        public int QuestionnaireId { get; set; }
        [StringLength(50)]
        public string FormName { get; set; }
        [Column("URL")]
        [StringLength(100)]
        public string Url { get; set; }
        [Column("UserID")]
        [StringLength(50)]
        public string UserId { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreateDate { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
    }
}
