﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Consent.Web.Models
{
    [Table("geography")]
    public partial class Geography
    {
        [Key]
        [Column("geography_id")]
        public int GeographyId { get; set; }
        [Required]
        [Column("geographyName")]
        [StringLength(50)]
        public string GeographyName { get; set; }
    }
}
