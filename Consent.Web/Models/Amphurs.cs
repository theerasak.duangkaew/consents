﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Consent.Web.Models
{
    [Table("amphurs")]
    public partial class Amphurs
    {
        [Key]
        [Column("amphurID")]
        public int AmphurId { get; set; }
        [Column("amphurcode")]
        public int Amphurcode { get; set; }
        [Required]
        [Column("amphurNameTH")]
        [StringLength(100)]
        public string AmphurNameTh { get; set; }
        [Column("provinceCode")]
        public int ProvinceCode { get; set; }
    }
}
