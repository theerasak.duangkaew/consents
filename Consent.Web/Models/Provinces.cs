﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Consent.Web.Models
{
    [Table("provinces")]
    public partial class Provinces
    {
        [Key]
        [Column("provinceID")]
        public int ProvinceId { get; set; }
        [Column("provinceCode")]
        public int ProvinceCode { get; set; }
        [Required]
        [Column("provinceNameTH")]
        [StringLength(150)]
        public string ProvinceNameTh { get; set; }
        [Column("geographyID")]
        public int GeographyId { get; set; }
    }
}
