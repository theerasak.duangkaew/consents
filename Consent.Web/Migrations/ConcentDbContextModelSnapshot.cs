﻿// <auto-generated />
using System;
using Consent.Web.Context;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace Consent.Web.Migrations
{
    [DbContext(typeof(ConcentDbContext))]
    partial class ConcentDbContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "3.1.10")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Consent.Web.Models.Amphurs", b =>
                {
                    b.Property<int>("AmphurId")
                        .HasColumnName("amphurID")
                        .HasColumnType("int");

                    b.Property<string>("AmphurNameTh")
                        .IsRequired()
                        .HasColumnName("amphurNameTH")
                        .HasColumnType("nvarchar(100)")
                        .HasMaxLength(100);

                    b.Property<int>("Amphurcode")
                        .HasColumnName("amphurcode")
                        .HasColumnType("int");

                    b.Property<int>("ProvinceCode")
                        .HasColumnName("provinceCode")
                        .HasColumnType("int");

                    b.HasKey("AmphurId")
                        .HasName("PK__amphurs__9B13B9854E5C1238");

                    b.ToTable("amphurs");
                });

            modelBuilder.Entity("Consent.Web.Models.Customer", b =>
                {
                    b.Property<string>("CustomerId")
                        .HasColumnType("varchar(50)")
                        .HasMaxLength(50)
                        .IsUnicode(false);

                    b.Property<string>("FirstName")
                        .HasColumnType("varchar(max)")
                        .IsUnicode(false);

                    b.Property<string>("LastName")
                        .HasColumnType("varchar(max)")
                        .IsUnicode(false);

                    b.Property<string>("Tel")
                        .HasColumnType("nvarchar(50)")
                        .HasMaxLength(50);

                    b.HasKey("CustomerId");

                    b.ToTable("customer");
                });

            modelBuilder.Entity("Consent.Web.Models.Geography", b =>
                {
                    b.Property<int>("GeographyId")
                        .HasColumnName("geography_id")
                        .HasColumnType("int");

                    b.Property<string>("GeographyName")
                        .IsRequired()
                        .HasColumnName("geographyName")
                        .HasColumnType("nvarchar(50)")
                        .HasMaxLength(50);

                    b.HasKey("GeographyId");

                    b.ToTable("geography");
                });

            modelBuilder.Entity("Consent.Web.Models.Provinces", b =>
                {
                    b.Property<int>("ProvinceId")
                        .HasColumnName("provinceID")
                        .HasColumnType("int");

                    b.Property<int>("GeographyId")
                        .HasColumnName("geographyID")
                        .HasColumnType("int");

                    b.Property<int>("ProvinceCode")
                        .HasColumnName("provinceCode")
                        .HasColumnType("int");

                    b.Property<string>("ProvinceNameTh")
                        .IsRequired()
                        .HasColumnName("provinceNameTH")
                        .HasColumnType("nvarchar(150)")
                        .HasMaxLength(150);

                    b.HasKey("ProvinceId")
                        .HasName("PK__province__20A1177F81155732");

                    b.ToTable("provinces");
                });

            modelBuilder.Entity("Consent.Web.Models.Questionnaire", b =>
                {
                    b.Property<int>("QuestionnaireId")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("QuestionnaireID")
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime?>("CreateDate")
                        .HasColumnType("datetime");

                    b.Property<DateTime?>("EndDate")
                        .HasColumnType("datetime2");

                    b.Property<string>("FormName")
                        .HasColumnType("nvarchar(50)")
                        .HasMaxLength(50);

                    b.Property<DateTime?>("StartDate")
                        .HasColumnType("datetime");

                    b.Property<string>("Url")
                        .HasColumnName("URL")
                        .HasColumnType("nvarchar(100)")
                        .HasMaxLength(100);

                    b.Property<string>("UserId")
                        .HasColumnName("UserID")
                        .HasColumnType("nvarchar(50)")
                        .HasMaxLength(50);

                    b.HasKey("QuestionnaireId");

                    b.ToTable("Questionnaire");
                });

            modelBuilder.Entity("Consent.Web.Models.Staff", b =>
                {
                    b.Property<string>("UserId")
                        .HasColumnName("UserID")
                        .HasColumnType("nvarchar(50)")
                        .HasMaxLength(50);

                    b.Property<DateTime?>("CreateDate")
                        .HasColumnType("datetime");

                    b.Property<string>("Email")
                        .HasColumnType("nvarchar(50)")
                        .HasMaxLength(50);

                    b.Property<string>("FirstName")
                        .HasColumnType("nvarchar(50)")
                        .HasMaxLength(50);

                    b.Property<DateTime?>("LastLogin")
                        .HasColumnType("datetime");

                    b.Property<string>("LastName")
                        .HasColumnType("nvarchar(50)")
                        .HasMaxLength(50);

                    b.Property<string>("Password")
                        .HasColumnType("nvarchar(50)")
                        .HasMaxLength(50);

                    b.Property<string>("Status")
                        .HasColumnType("nvarchar(50)")
                        .HasMaxLength(50);

                    b.Property<string>("UserGroup")
                        .HasColumnType("nvarchar(50)")
                        .HasMaxLength(50);

                    b.Property<string>("UserRole")
                        .HasColumnType("nvarchar(50)")
                        .HasMaxLength(50);

                    b.HasKey("UserId");

                    b.ToTable("Staff");
                });

            modelBuilder.Entity("Consent.Web.Models.Tambons", b =>
                {
                    b.Property<int>("TamId")
                        .HasColumnName("tam_id")
                        .HasColumnType("int");

                    b.Property<int>("AmphurCode")
                        .HasColumnName("amphurCode")
                        .HasColumnType("int");

                    b.Property<int>("TambonCode")
                        .HasColumnName("tambonCode")
                        .HasColumnType("int");

                    b.Property<string>("TambonNameTh")
                        .IsRequired()
                        .HasColumnName("tambonNameTH")
                        .HasColumnType("nvarchar(50)")
                        .HasMaxLength(50);

                    b.Property<int>("Zipcode")
                        .HasColumnName("zipcode")
                        .HasColumnType("int");

                    b.HasKey("TamId")
                        .HasName("PK__tambons__39E67F39C28E3D28");

                    b.ToTable("tambons");
                });

            modelBuilder.Entity("Consent.Web.Models.Title", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("TitleCode")
                        .HasColumnType("nvarchar(4)")
                        .HasMaxLength(4);

                    b.Property<string>("TitleName")
                        .HasColumnType("nvarchar(20)")
                        .HasMaxLength(20);

                    b.HasKey("Id");

                    b.ToTable("Titles");
                });

            modelBuilder.Entity("Consent.Web.Models.User", b =>
                {
                    b.Property<string>("Idcard")
                        .HasColumnName("IDCard")
                        .HasColumnType("nvarchar(13)")
                        .HasMaxLength(13);

                    b.Property<DateTime?>("BirthDate")
                        .HasColumnType("date");

                    b.Property<string>("BranchCode")
                        .HasColumnType("nvarchar(50)")
                        .HasMaxLength(50);

                    b.Property<string>("BuildingCode")
                        .HasColumnType("nvarchar(50)")
                        .HasMaxLength(50);

                    b.Property<string>("BusinessPartner")
                        .HasColumnType("nvarchar(50)")
                        .HasMaxLength(50);

                    b.Property<bool?>("Consent")
                        .HasColumnType("bit");

                    b.Property<string>("Description")
                        .HasColumnType("nvarchar(100)")
                        .HasMaxLength(100);

                    b.Property<string>("District")
                        .HasColumnType("nvarchar(50)")
                        .HasMaxLength(50);

                    b.Property<string>("Email")
                        .HasColumnType("nvarchar(50)")
                        .HasMaxLength(50);

                    b.Property<string>("Extension")
                        .HasColumnType("nvarchar(50)")
                        .HasMaxLength(50);

                    b.Property<string>("FacebookId")
                        .HasColumnName("FacebookID")
                        .HasColumnType("nvarchar(50)")
                        .HasMaxLength(50);

                    b.Property<string>("FirstName")
                        .HasColumnType("nvarchar(50)")
                        .HasMaxLength(50);

                    b.Property<string>("Floor")
                        .HasColumnType("nvarchar(50)")
                        .HasMaxLength(50);

                    b.Property<string>("Gmail")
                        .HasColumnType("nvarchar(50)")
                        .HasMaxLength(50);

                    b.Property<string>("HouseNumber")
                        .HasColumnType("nvarchar(50)")
                        .HasMaxLength(50);

                    b.Property<string>("KubotaId")
                        .HasColumnName("KubotaID")
                        .HasColumnType("nvarchar(50)")
                        .HasMaxLength(50);

                    b.Property<string>("LastName")
                        .HasColumnType("nvarchar(50)")
                        .HasMaxLength(50);

                    b.Property<string>("LineId")
                        .HasColumnName("LineID")
                        .HasColumnType("nvarchar(50)")
                        .HasMaxLength(50);

                    b.Property<string>("MiddleName")
                        .HasColumnType("nvarchar(50)")
                        .HasMaxLength(50);

                    b.Property<string>("MobilePhone1")
                        .HasColumnType("nvarchar(10)")
                        .HasMaxLength(10);

                    b.Property<string>("MobilePhone2")
                        .HasColumnType("nvarchar(10)")
                        .HasMaxLength(10);

                    b.Property<string>("Notes")
                        .HasColumnType("nvarchar(50)")
                        .HasMaxLength(50);

                    b.Property<string>("Province")
                        .HasColumnType("nvarchar(50)")
                        .HasMaxLength(50);

                    b.Property<string>("RoomNumber")
                        .HasColumnType("nvarchar(50)")
                        .HasMaxLength(50);

                    b.Property<string>("SearchTerm")
                        .HasColumnType("nvarchar(50)")
                        .HasMaxLength(50);

                    b.Property<string>("SignatureImg")
                        .HasColumnType("text");

                    b.Property<string>("Source")
                        .HasColumnType("nvarchar(50)")
                        .HasMaxLength(50);

                    b.Property<string>("Street")
                        .HasColumnType("nvarchar(50)")
                        .HasMaxLength(50);

                    b.Property<string>("Street2")
                        .HasColumnType("nvarchar(50)")
                        .HasMaxLength(50);

                    b.Property<string>("Street4")
                        .HasColumnType("nvarchar(50)")
                        .HasMaxLength(50);

                    b.Property<string>("Supplement")
                        .HasColumnType("nvarchar(50)")
                        .HasMaxLength(50);

                    b.Property<string>("Tambon")
                        .HasColumnType("nvarchar(50)")
                        .HasMaxLength(50);

                    b.Property<string>("TelephoneNo")
                        .HasColumnType("nvarchar(10)")
                        .HasMaxLength(10);

                    b.Property<string>("Title")
                        .HasColumnType("nvarchar(50)")
                        .HasMaxLength(50);

                    b.Property<string>("ZipCode")
                        .HasColumnType("nvarchar(5)")
                        .HasMaxLength(5);

                    b.HasKey("Idcard");

                    b.ToTable("User");
                });
#pragma warning restore 612, 618
        }
    }
}
