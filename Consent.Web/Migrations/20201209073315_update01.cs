﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Consent.Web.Migrations
{
    public partial class update01 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.CreateTable(
            //    name: "amphurs",
            //    columns: table => new
            //    {
            //        amphurID = table.Column<int>(nullable: false),
            //        amphurcode = table.Column<int>(nullable: false),
            //        amphurNameTH = table.Column<string>(maxLength: 100, nullable: false),
            //        provinceCode = table.Column<int>(nullable: false)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK__amphurs__9B13B9854E5C1238", x => x.amphurID);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "customer",
            //    columns: table => new
            //    {
            //        CustomerId = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
            //        FirstName = table.Column<string>(unicode: false, nullable: true),
            //        LastName = table.Column<string>(unicode: false, nullable: true),
            //        Tel = table.Column<string>(maxLength: 50, nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_customer", x => x.CustomerId);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "geography",
            //    columns: table => new
            //    {
            //        geography_id = table.Column<int>(nullable: false),
            //        geographyName = table.Column<string>(maxLength: 50, nullable: false)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_geography", x => x.geography_id);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "provinces",
            //    columns: table => new
            //    {
            //        provinceID = table.Column<int>(nullable: false),
            //        provinceCode = table.Column<int>(nullable: false),
            //        provinceNameTH = table.Column<string>(maxLength: 150, nullable: false),
            //        geographyID = table.Column<int>(nullable: false)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK__province__20A1177F81155732", x => x.provinceID);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "Questionnaire",
            //    columns: table => new
            //    {
            //        QuestionnaireID = table.Column<int>(nullable: false)
            //            .Annotation("SqlServer:Identity", "1, 1"),
            //        FormName = table.Column<string>(maxLength: 50, nullable: true),
            //        URL = table.Column<string>(maxLength: 100, nullable: true),
            //        UserID = table.Column<string>(maxLength: 50, nullable: true),
            //        CreateDate = table.Column<DateTime>(type: "datetime", nullable: true),
            //        StartDate = table.Column<DateTime>(type: "datetime", nullable: true),
            //        EndDate = table.Column<DateTime>(nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_Questionnaire", x => x.QuestionnaireID);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "Staff",
            //    columns: table => new
            //    {
            //        UserID = table.Column<string>(maxLength: 50, nullable: false),
            //        FirstName = table.Column<string>(maxLength: 50, nullable: true),
            //        LastName = table.Column<string>(maxLength: 50, nullable: true),
            //        Email = table.Column<string>(maxLength: 50, nullable: true),
            //        Password = table.Column<string>(maxLength: 50, nullable: true),
            //        UserGroup = table.Column<string>(maxLength: 50, nullable: true),
            //        UserRole = table.Column<string>(maxLength: 50, nullable: true),
            //        CreateDate = table.Column<DateTime>(type: "datetime", nullable: true),
            //        LastLogin = table.Column<DateTime>(type: "datetime", nullable: true),
            //        Status = table.Column<string>(maxLength: 50, nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_Staff", x => x.UserID);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "tambons",
            //    columns: table => new
            //    {
            //        tam_id = table.Column<int>(nullable: false),
            //        tambonCode = table.Column<int>(nullable: false),
            //        tambonNameTH = table.Column<string>(maxLength: 50, nullable: false),
            //        zipcode = table.Column<int>(nullable: false),
            //        amphurCode = table.Column<int>(nullable: false)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK__tambons__39E67F39C28E3D28", x => x.tam_id);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "User",
            //    columns: table => new
            //    {
            //        IDCard = table.Column<string>(maxLength: 13, nullable: false),
            //        BusinessPartner = table.Column<string>(maxLength: 50, nullable: true),
            //        KubotaID = table.Column<string>(maxLength: 50, nullable: true),
            //        Title = table.Column<string>(maxLength: 50, nullable: true),
            //        FirstName = table.Column<string>(maxLength: 50, nullable: true),
            //        MiddleName = table.Column<string>(maxLength: 50, nullable: true),
            //        LastName = table.Column<string>(maxLength: 50, nullable: true),
            //        BirthDate = table.Column<DateTime>(type: "date", nullable: true),
            //        SearchTerm = table.Column<string>(maxLength: 50, nullable: true),
            //        BranchCode = table.Column<string>(maxLength: 50, nullable: true),
            //        Description = table.Column<string>(maxLength: 100, nullable: true),
            //        HouseNumber = table.Column<string>(maxLength: 50, nullable: true),
            //        RoomNumber = table.Column<string>(maxLength: 50, nullable: true),
            //        Floor = table.Column<string>(maxLength: 50, nullable: true),
            //        BuildingCode = table.Column<string>(maxLength: 50, nullable: true),
            //        Supplement = table.Column<string>(maxLength: 50, nullable: true),
            //        Street2 = table.Column<string>(maxLength: 50, nullable: true),
            //        Street4 = table.Column<string>(maxLength: 50, nullable: true),
            //        Street = table.Column<string>(maxLength: 50, nullable: true),
            //        Tambon = table.Column<string>(maxLength: 50, nullable: true),
            //        District = table.Column<string>(maxLength: 50, nullable: true),
            //        Province = table.Column<string>(maxLength: 50, nullable: true),
            //        ZipCode = table.Column<string>(maxLength: 5, nullable: true),
            //        TelephoneNo = table.Column<string>(maxLength: 10, nullable: true),
            //        Extension = table.Column<string>(maxLength: 50, nullable: true),
            //        MobilePhone1 = table.Column<string>(maxLength: 10, nullable: true),
            //        MobilePhone2 = table.Column<string>(maxLength: 10, nullable: true),
            //        Email = table.Column<string>(maxLength: 50, nullable: true),
            //        FacebookID = table.Column<string>(maxLength: 50, nullable: true),
            //        LineID = table.Column<string>(maxLength: 50, nullable: true),
            //        Gmail = table.Column<string>(maxLength: 50, nullable: true),
            //        Notes = table.Column<string>(maxLength: 50, nullable: true),
            //        Consent = table.Column<bool>(nullable: true),
            //        Source = table.Column<string>(maxLength: 50, nullable: true),
            //        SignatureImg = table.Column<string>(type: "text", nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_User", x => x.IDCard);
            //    });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "amphurs");

            migrationBuilder.DropTable(
                name: "customer");

            migrationBuilder.DropTable(
                name: "geography");

            migrationBuilder.DropTable(
                name: "provinces");

            migrationBuilder.DropTable(
                name: "Questionnaire");

            migrationBuilder.DropTable(
                name: "Staff");

            migrationBuilder.DropTable(
                name: "tambons");

            migrationBuilder.DropTable(
                name: "User");
        }
    }
}
