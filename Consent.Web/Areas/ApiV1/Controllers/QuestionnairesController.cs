﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Consent.Web.Context;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Consent.Web.Areas.ApiV1.Controllers
{
    [Area("v1")]
    [ApiExplorerSettings(GroupName = "v1")]
    [Produces("application/json")]
    [Route("api/v1/[controller]")]
    [ApiController]
    public class QuestionnairesController : ControllerBase
    {
        private readonly ConcentDbContext _db;

        public QuestionnairesController(ConcentDbContext db)
        {
            _db = db;
        }

        [HttpGet]
        public async Task<ActionResult> GetQuestionnairesAsync()
        {
            var quiz = await _db.Questionnaire.ToListAsync();
            return Ok(quiz);
        }
    }
}
