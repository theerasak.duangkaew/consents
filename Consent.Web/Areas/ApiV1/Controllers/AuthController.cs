﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Consent.Web.Areas.ApiV1.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;

namespace Consent.Web.Areas.ApiV1.Controllers
{
    [Area("v1")]
    [ApiExplorerSettings(GroupName = "v1")]
    [Produces("application/json")]
    [Route("api/v1/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        //private readonly SignInManager<IdentityUser> _signInManager;
        //private readonly UserManager<IdentityUser> _userManager;
        //public AuthController(SignInManager<IdentityUser> signInManager, UserManager<IdentityUser> userManager)
        //{
        //    _signInManager = signInManager;
        //    _userManager = userManager;
        //}

        [Authorize]
        [HttpGet("CreateToken")]
        public ActionResult CreateTokenAsync()
        {
            var output = new AuthResponse();

            if (!User.Identity.IsAuthenticated)
                return BadRequest();

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("siamkubota.co.th"));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(
                issuer: "siamkubota.co.th",
                audience: "siamkubota.co.th",
                claims: User.Claims,
                expires: DateTime.Now.AddYears(30),
                signingCredentials: creds);

            output.Token = new JwtSecurityTokenHandler().WriteToken(token);
            return Ok(output);
        }


        [Authorize]
        [HttpGet("Me")]
        public ActionResult GetMeAsync()
        {
            if (!User.Identity.IsAuthenticated)
                return Unauthorized();

            return Ok(User.Identity.Name);
        }
    }
}
