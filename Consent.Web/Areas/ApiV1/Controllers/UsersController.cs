﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Consent.Web.Context;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Consent.Web.Areas.ApiV1.Controllers
{
    [Area("v1")]
    [ApiExplorerSettings(GroupName = "v1")]
    [Produces("application/json")]
    [Route("api/v1/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly ConcentDbContext _db;

        public UsersController(ConcentDbContext db)
        {
            _db = db;
        }

        [HttpGet("Customers/Titles")]
        public async Task<ActionResult> GetTitlesAsync()
        {
            var titles = await _db.Titles.ToListAsync();
            return Ok(titles);
        }


        [HttpGet("Staff")]
        public async Task<ActionResult> GetStaffAsync()
        {
            var staff = await _db.Staff.ToListAsync();
            return Ok(staff);
        }

        [HttpGet("Customers")]
        public async Task<ActionResult> GetCustomersAsync()
        {
            var customers = await _db.User.ToListAsync();
            return Ok(customers);
        }

        [HttpPost("Consents")]
        public async Task<ActionResult> SetConsentsAsync()
        {
            var customers = await _db.User.ToListAsync();
            return Ok(customers);
        }
    }
}
