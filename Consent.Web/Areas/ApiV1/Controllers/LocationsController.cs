﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Consent.Web.Context;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Consent.Web.Areas.ApiV1.Controllers
{
    [Area("v1")]
    [ApiExplorerSettings(GroupName = "v1")]
    [Produces("application/json")]
    [Route("api/v1/[controller]")]
    [ApiController]
    public class LocationsController : ControllerBase
    {
        private readonly ConcentDbContext _db;

        public LocationsController(ConcentDbContext db)
        {
            _db = db;
        }

        [HttpGet("Provinces")]
        public async Task<ActionResult> GetProvincesAsync()
        {
            var provinces = await _db.Provinces.ToListAsync();
            return Ok(provinces);
        }

        [HttpGet("Amphurs/{ProvinceCode}")]
        public async Task<ActionResult> GetAmphursAsync(int ProvinceCode)
        {
            var amphurs = await _db.Amphurs.Where(x => x.ProvinceCode == ProvinceCode).ToListAsync();
            return Ok(amphurs);
        }

        [HttpGet("Tambons/{AmphurCode}")]
        public async Task<ActionResult> GetTambonsAsync(int AmphurCode)
        {
            var tambons = await _db.Tambons.Where(x => x.AmphurCode == AmphurCode).ToListAsync();
            return Ok(tambons);
        }
    }
}
