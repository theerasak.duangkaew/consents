﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Consent.Web.Areas.ApiV1.Models
{
    public class ConsentAgreement
    {
        public int IdCard { get; set; }
        public string Base64Image { get; set; }

    }
}
