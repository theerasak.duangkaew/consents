﻿using System.ComponentModel.DataAnnotations;

namespace Consent.Web.Areas.ApiV1.Models
{
    public class AuthRequest
    {
        [Required]
        [StringLength(30)]
        public string Username { get; set; }

        [Required]
        [StringLength(30)]
        public string Password { get; set; }
    }
}